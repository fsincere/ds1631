# -*- coding: utf-8 -*-
# python 3
# all operating systems
# (C) Fabrice Sincère

import DS1631
import time

# virtual device
room = DS1631.DS1631virtualdevice(initial_temperature=18,
                                  thigh=20, tlow=19,
                                  P=1000, R=0.02,
                                  tau=360,
                                  Text_max=15, Text_min=10,
                                  period=24*3600)
while True:
    time.sleep(0.75)
    temperature = room.get_temperature()
    print("Temperature  : {} °C".format(temperature))
