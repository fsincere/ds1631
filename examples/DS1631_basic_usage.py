# -*- coding: utf-8 -*-
# python 3
# (C) Fabrice Sincère

import DS1631
import time

i2c_address = 0x48
ic1 = DS1631.DS1631(1, i2c_address)
# thermostat config
ic1.set_tout_polarity("active-low")
ic1.set_thigh(22.5)
ic1.set_tlow(20.5)
# thermometer config
ic1.set_conversion_mode("continuous")
ic1.set_resolution(12)
ic1.start_convert()
# read temperature
while True:
    time.sleep(0.75)
    temperature = ic1.get_temperature()
    print("Temperature  : {} °C".format(temperature))
