# -*- coding: utf-8 -*-
# python 3
# Raspberry pi (OS Raspbian)
# (C) Fabrice Sincère

# a DS1631 device is connected to Raspberry pi i2c GPIO with 4 wires :
# SDA i2c bus (GPIO2)
# SCL i2c bus (GPIO3)
# Power (3.3 V or 5 V)
# Ground

# a LED is attached to DS1631 Tout pin
# (LED + 270 ohms resistor between Tout pin and ground)

import time
import DS1631

print(DS1631.__version__)

i2c_address = 0x48
ic1 = DS1631.DS1631(1, i2c_address)

ic1.software_por()
ic1.stop_convert()

ic1.print_configuration()

assert ic1.get_temperature() == -60.0
assert ic1.get_resolution()[0] == 12
assert ic1.is_thigh_flag_on() is False
assert ic1.is_tlow_flag_on() is False

ic1.start_convert()
time.sleep(ic1.get_resolution()[2]*0.001)
assert ic1.get_temperature() != -60.0

for i in range(9, 12+1):
    ic1.set_resolution(i)
    assert ic1.get_resolution()[0] == i

for i in ["one-shot", "continuous"]:
    ic1.set_conversion_mode(i)
    assert ic1.get_conversion_mode() == i

for i in ["active-high", "active-low"]:
    ic1.set_tout_polarity(i)
    assert ic1.get_tout_polarity() == i

ic1.stop_convert()
time.sleep(ic1.get_resolution()[2]*0.001)
ic1.reset_thigh_flag()
ic1.reset_tlow_flag()
ic1.set_thigh(125.0)
assert ic1.get_thigh() == 125.0
ic1.set_tlow(124.0)
assert ic1.get_tlow() == 124.0
ic1.start_convert()
time.sleep(ic1.get_resolution()[2]*0.001)
print(ic1.get_temperature())
assert ic1.is_thigh_flag_on() is False
assert ic1.is_tlow_flag_on() is True
input("Tout = 1\nEnter to continue...")

ic1.stop_convert()
time.sleep(ic1.get_resolution()[2]*0.001)
ic1.reset_thigh_flag()
ic1.reset_tlow_flag()
ic1.set_thigh(-54.0)
assert ic1.get_thigh() == -54.0
ic1.set_tlow(-55.0)
assert ic1.get_tlow() == -55.0
ic1.start_convert()
time.sleep(ic1.get_resolution()[2]*0.001)
print(ic1.get_temperature())
assert ic1.is_thigh_flag_on() is True
assert ic1.is_tlow_flag_on() is False
input("Tout = 0\nEnter to continue...")

ic1.stop_convert()
time.sleep(ic1.get_resolution()[2]*0.001)
ic1.reset_thigh_flag()
ic1.reset_tlow_flag()
ic1.set_thigh(125.0)
assert ic1.get_thigh() == 125.0
ic1.set_tlow(124.0)
assert ic1.get_tlow() == 124.0
ic1.start_convert()
time.sleep(ic1.get_resolution()[2]*0.001)
print(ic1.get_temperature())
assert ic1.is_thigh_flag_on() is False
assert ic1.is_tlow_flag_on() is True
input("Tout = 1\nEnter to continue...")

ic1.stop_convert()
ic1.set_tout_polarity("active-high")
ic1.start_convert()
time.sleep(ic1.get_resolution()[2]*0.001)
print(ic1.get_temperature())
input("Tout = 0\nEnter to continue...")

ic1.stop_convert()
ic1.set_tout_polarity("active-low")
ic1.start_convert()
time.sleep(ic1.get_resolution()[2]*0.001)
print(ic1.get_temperature())
input("Tout = 1\nEnter to continue...")

ic1.stop_convert()
ic1.set_conversion_mode("one-shot")
ic1.set_resolution(12)
time.sleep(ic1.get_resolution()[2]*0.001)
count = 0
start = time.time()
ic1.start_convert()
while ic1.is_temperature_conversion_in_progress():
    count += 1
end = time.time()
print('Conversion time', end-start, 's')
print(count, 'i2c requests')
print(ic1.get_temperature())

ic1.stop_convert()
count = 0
start = time.time()
ic1.set_thigh(63.42)
while ic1.is_eeprom_busy():
    count += 1
end = time.time()
print('EEPROM write time', end-start, 's')
print(count, 'i2c requests')
print(ic1.get_thigh())

ic1.stop_convert()
count = 0
start = time.time()
ic1.reset_tlow_flag()
while ic1.is_eeprom_busy():
    count += 1
end = time.time()
print('EEPROM write time', end-start, 's')
print(count, 'i2c requests')

print("Test OK")

""" result example :
(0, 3, 7)

Configuration
-------------
I2c address            : 0x48
Configuration register : 0x8c

Conversion mode        : continuous
Tout polarity          : active-low
Resolution             : 12 bits
 --> Resolution        : 0.0625 °C
 --> Conversion time   : 750.0 ms (max)

Status information
------------------
EEPROM memory          : memory is not busy
Temperature Low Flag   : off
Temperature High Flag  : off
Temperature conversion : complete

28.0625
Tout = 1
Enter to continue...
28.0
Tout = 0
Enter to continue...
28.0
Tout = 1
Enter to continue...
28.0625
Tout = 0
Enter to continue...
28.0
Tout = 1
Enter to continue...
Conversion time 0.569826602935791 s
820 i2c requests
28.0
EEPROM write time 0.00278472900390625 s
2 i2c requests
63.375
EEPROM write time 0.003294229507446289 s
2 i2c requests
Test OK
"""
